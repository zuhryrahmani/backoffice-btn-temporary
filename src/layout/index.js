// main
import React, { useState, useEffect } from 'react';
import { Redirect} from 'react-router-dom';

// libraries
import { Layout } from 'antd';

// components
import SideBar from '../components/TemplateLayout/SideBar';
import Header from '../components/TemplateLayout/Header';
import IdleTimerContainer from "../components/IdleContainerTimer";

const { Content } = Layout;

const Container = ({children}) => {

  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    if(!sessionStorage.getItem('username')) {
      setRedirect(<Redirect to='/' />);
    };
  }, [sessionStorage]);

  return (
      <Layout style={{ height: '100vh', background: '#fff' }}>
        <SideBar />
        <Layout style={{backgroundColor:'#F9FAFF'}}>
          <Content>
            <Header />
            {children}
            <IdleTimerContainer></IdleTimerContainer>
          </Content>
        </Layout>
        {redirect}
      </Layout>
  );
  
};

export default Container;
