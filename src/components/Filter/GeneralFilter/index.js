// main
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles } from '@material-ui/core';

// components
import Button from '../../Button/GeneralButton';
import Tabs from '../../Tabs/GeneralTabs';
import DatePicker from '../../DatePicker/RangeDatePicker';

const GeneralFilter = (props) => {

  const { options, className, align, dataFilter, setDataFilter } = props;
  const useStyles = makeStyles({
    filter: {
      width: '100%',
      minHeight: 70,
      backgroundColor: '#fff',
      borderRadius: 8,
      display: 'flex',
      position: 'relative'
    },
    title: {
      fontFamily: 'FuturaMdBT',
      fontWeight: 600,
      marginLeft: 15,
      lineHeight: '73px',
      fontSize: 16
    },
    button: {
      lineHeight: '73px',
      position: 'absolute',
      right: 20,
      top: 0
    },
    container: {
      display: 'flex',
      flex: 'auto',
      paddingRight: 130,
      justifyContent: align === 'center' ? 'center' : 'flex-start'
    },
    inputContainer: {
      display: 'flex',
      lineHeight: '73px',
      alignItems: 'center',
      margin: align === 'center' ? '0 10px' : '0 0 0 20px'
    },
    input: {
      marginLeft: 10,
    },
  });
  const classes = useStyles();

  const [tabValue, setTabValue] = useState(null);
  const [dateValue, setDateValue] = useState(null);
  // const [openCalendar, setOpenCalendar] = useState(null);
  // const [placeholderState, setPlaceholderState] = useState(null);
  // console.log('LOOK VALUE', dateValue);
  // console.log('LOOK OPEN', openCalendar);
  // console.log('LOOK PLACE', placeholderState);

  const handleFilter = () => {
    setDataFilter({
      tabs: tabValue,
      date: dateValue
    });
  };

  useEffect(() => {
    if(options.length>0) {
      const objTabs = {};
      const objDate = {};
      // const objPlace = {};
      options.map(item => {
        if(item.type==='tabs') {
          objTabs[`tabValue${item.id}`] = tabValue ? tabValue[`tabValue${item.id}`] : 0;
        } else {
          objDate[`dateValue${item.id}`] = dateValue && dateValue[`dateValue${item.id}`];
          // objPlace[`placeholderState${item.id}`] = true;
        };
      });
      setTabValue(objTabs);
      setDateValue(objDate);
      // setPlaceholderState(objPlace);
    };
  }, [options]);

  return(
    <div className={className ? `${className} ${classes.filter}` : classes.filter}>
      <div className={classes.title}>
        Tampilkan :
      </div>
      <div className={classes.container}>
        {options.map((item, i) => (
          item.type==='tabs' ? (
            <div className={classes.inputContainer}>
              <Tabs
                value={tabValue && tabValue[`tabValue${item.id}`]}
                onChange={tabValue && ((event, newValue) => setTabValue({...tabValue, [`tabValue${item.id}`]: newValue}))}
                tabs={item.options}
              />
            </div>
          ) : item.type==='date' ? (
            <div className={classes.inputContainer}>
              <div>
                {item.title==='' || item.title===undefined ? '' : `${item.title} :`}
              </div>
              <div className={classes.input}>
                <DatePicker
                  // openCalendar={openCalendar}
                  // setOpenCalendar={setOpenCalendar}
                  // placeholderState={placeholderState}
                  // setPlaceholderState={setPlaceholderState}
                  datePickerID={item.id}
                  value={dateValue && dateValue[`dateValue${item.id}`]}
                  onChange={dateValue && ((dates, dateStrings) => setDateValue({...dateValue, [`dateValue${item.id}`]: dates}))}
                  placeholder={item.placeholder}
                />
              </div>
              {item.dash && (
                <div className={classes.range}>
                  {item.dash}
                </div>
              )}
            </div>
          ) : null
        ))}
      </div>
      <div className={classes.button}>
        <Button
          label='Terapkan'
          width='104px'
          height='40px'
          onClick={handleFilter}
        />
      </div>
    </div>
  );
};

GeneralFilter.propTypes = {
  options: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.string,
  dataFilter: PropTypes.object.isRequired,
  setDataFilter: PropTypes.func.isRequired
};

GeneralFilter.defaultProps = {
  options: [],
  className: '',
  align: 'left'
};

export default GeneralFilter;
