// main
import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';

// libraries
import {
  makeStyles, AppBar, Toolbar, IconButton,
  ClickAwayListener, Grow, Paper, Popper, MenuItem, MenuList
} from '@material-ui/core';

// assets
import logo from '../../../assets/images/logo-btn.png';
import bell from '../../../assets/icons/bell.svg';
import bellDot from '../../../assets/icons/bell-dot.svg';
import refresh from '../../../assets/icons/refresh-cw.svg';
import arrow from '../../../assets/icons/chevron-down.svg';
import envelope from '../../../assets/icons/envelope.svg';
import envelopeOpen from '../../../assets/icons/envelope-open.svg';
import logout from '../../../assets/icons/log-out.svg';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    height: '60px',
    borderBottom: '1px solid #E6EAF3',
    boxShadow: 'none',
    fontFamily: 'FuturaMdBT'
  },
  logo: {
    height: 28,
    margin: '16px 20px',
    cursor: 'pointer'
  },
  navbar: {
    position: 'absolute',
    right: 20,
    top: '47%',
    transform: 'translateY(-50%)',
    color: '#0061A7',
    fontSize: 14,
    paddingRight: 42,
    display: 'flex'
  },
  icon: {
    marginRight: 8,
  },
  avatar: {
    borderRadius: '50%',
    height: 32,
    width: 32,
    border: '3px solid #FFF',
    boxShadow: '0px 10px 10px rgba(193, 207, 227, 0.24)',
    backgroundColor: '#0061A7',
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
    position: 'absolute',
    right: 0,
    top: '50%',
    transform: 'translateY(-50%)'
  },
  notificationPaper: {
    borderRadius: '0px 0px 10px 10px',
    boxShadow: '0px 6px 16px rgba(179, 202, 244, 0.18)'
  },
  notification: {
    marginTop: 20,
    width: 280,
    height: 300,
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      display: 'none'
    },
    '& li': {
      padding: 0,
      '&:hover': {
        backgroundColor: '#F4F7FB'
      }
    }
  },
  notif: {
    width: '100%',
    height: 60,
    boxShadow: 'inset 0px -1px 0px #E6EAF3',
    padding: '8px 16px 6px',
    position: 'relative'
  },
  envelope: {
    position: 'absolute',
    top: 14,
    right: 10
  },
  profilePosition: {
    left: '42px !important'
  },
  profilePaper: {
    marginTop: 6,
    borderRadius: 8,
    boxShadow: 'none',
    border: '1px solid #BCC8E7'
  },
  profile: {
    width: 176,
    height: 135,
    padding: '6px 8px'
  },
  profileContent: {
    fontSize: 10,
    marginBottom: 6,
    fontFamily: 'FuturaBkBT'
  },
  profileDot: {
    borderRadius: '50%',
    width: 10,
    height: 10,
    backgroundColor: '#75D37F',
    margin: '3px 5px 0 0'
  },
  profileContentLast: {
    fontSize: 10,
    borderTop: '1px solid #BCC8E7',
    color: '#FF6F6F',
    fontWeight: 900,
    paddingTop: 3,
    position: 'relative'
  },
  logout: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    cursor: 'pointer'
  }
});

const Header = () => {

  const classes = useStyles();
  const history = useHistory();

  const [bellValue, setBellValue] = useState(true);

  // dropdown
  const [openNotif, setOpenNotif] = useState(false);
  const [openProfile, setOpenProfile] = useState(false);
  const anchorRefNotif = useRef(null);
  const anchorRefProfile = useRef(null);

  const dummyNotif = [
    {
      title: 'Terdapat 2 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
    {
      title: 'Terdapat 5 Transaksi Gagal',
      date: '26/12/2021',
      status: 1
    },
    {
      title: 'Terdapat 2 Transaksi Gagal',
      date: '26/12/2021',
      status: 1
    },
    {
      title: 'Terdapat 3 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
    {
      title: 'Terdapat 10 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
    {
      title: 'Terdapat 2 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
    {
      title: 'Terdapat 5 Transaksi Gagal',
      date: '26/12/2021',
      status: 1
    },
    {
      title: 'Terdapat 2 Transaksi Gagal',
      date: '26/12/2021',
      status: 1
    },
    {
      title: 'Terdapat 3 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
    {
      title: 'Terdapat 10 Transaksi Gagal',
      date: '26/12/2021',
      status: 0
    },
  ];

  const handleToggleNotif = () => {
    setOpenNotif((prevOpenNotif) => !prevOpenNotif);
  };
  const handleToggleProfile = () => {
    setOpenProfile((prevOpenProfile) => !prevOpenProfile);
  };

  const handleCloseNotif = (event) => {
    if (anchorRefNotif.current && anchorRefNotif.current.contains(event.target)) {
      return;
    };
    setOpenNotif(false);
  };
  const handleCloseProfile = (event) => {
    if (anchorRefProfile.current && anchorRefProfile.current.contains(event.target)) {
      return;
    };
    setOpenProfile(false);
  };

  const handleListKeyDownNotif = (event) => {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpenNotif(false);
    };
  };
  const handleListKeyDownProfile = (event) => {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpenProfile(false);
    };
  };
  const handleLogout = () => {
    sessionStorage.clear();
    window.location.replace('/');
  };

  const prevOpenNotif = useRef(openNotif);
  const prevOpenProfile = useRef(openProfile);

  useEffect(() => {
    if (prevOpenNotif.current === true && openNotif === false) {
      anchorRefNotif.current.focus();
    }
    prevOpenNotif.current = openNotif;
  }, [openNotif]);

  useEffect(() => {
    if (prevOpenProfile.current === true && openProfile === false) {
      anchorRefProfile.current.focus();
    }
    prevOpenProfile.current = openProfile;
  }, [openProfile]);

  return (
    <AppBar className={classes.root}>
      <Toolbar style={{padding: 0}}>
        <img src={logo} alt='BTN Logo' className={classes.logo} onClick={() => history.push('/ringkasan')} />
        <div className={classes.navbar}>
          <div className={classes.icon} style={{position:'relative', zIndex:2}}>
            <IconButton
              color='primary'
              size='small'
              ref={anchorRefNotif}
              aria-controls={openNotif ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => setOpenNotif(!openNotif)}
              style={{cursor:'pointer'}}
            >
              <img src={bellValue ? bellDot : bell} alt='Notification' />
            </IconButton>
            <Popper open={openNotif} anchorEl={anchorRefNotif.current} role={undefined} transition disablePortal>
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper className={classes.notificationPaper}>
                    <ClickAwayListener onClickAway={handleCloseNotif}>
                      <MenuList autoFocusItem={openNotif} id="menu-list-grow" onKeyDown={handleListKeyDownNotif} className={classes.notification}>
                        {dummyNotif.map(item => (
                          <MenuItem>
                            <div className={classes.notif}>
                              <p style={{fontSize:16, marginBottom:3, fontFamily:'FuturaMdBT'}}>{item.title}</p>
                              <p style={{fontSize:12, color:'#BCC8E7'}}>{item.date}</p>
                              <img src={item.status===0 ? envelopeOpen : envelope} alt={item.status===0 ? 'Opened Envelope' : 'Closed Envelope'} className={classes.envelope}/>
                            </div>
                          </MenuItem>
                        ))}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
          <div className={classes.icon}>
            <IconButton color='primary' size='small' style={{cursor:'pointer'}}>
              <img src={refresh} alt='Refresh' />
            </IconButton>
          </div>
          <div className={classes.icon} style={{position:'relative', zIndex:1}}>
            <IconButton
              color='primary'
              size='small'
              ref={anchorRefProfile}
              aria-controls={openProfile ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => setOpenProfile(!openProfile)}
              style={{cursor:'pointer'}}
            >
              <img src={arrow} alt='Profile' />
            </IconButton>
            <Popper open={openProfile} anchorEl={anchorRefProfile.current} role={undefined} transition disablePortal className={classes.profilePosition}>
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper className={classes.profilePaper}>
                    <ClickAwayListener onClickAway={handleCloseProfile}>
                      <MenuList autoFocusItem={openProfile} id="menu-list-grow" onKeyDown={handleListKeyDownProfile} className={classes.profile}>
                        <div className={classes.profileContent} style={{fontWeight:700, fontFamily:'Futura'}}>Peter Santoso</div>
                        <div className={classes.profileContent}>Admin</div>
                        <div className={classes.profileContent}>Head Office</div>
                        <div className={classes.profileContent} style={{display:'flex'}}>
                          <div className={classes.profileDot}></div>
                          <div>Last Login 12/06/21 |20:21:43</div>
                        </div>
                        <div className={classes.profileContent}>192.168.210.10</div>
                        <div className={classes.profileContentLast}>
                          <span style={{fontFamily:'FuturaHvBT'}}>Logout</span>
                          <img src={logout} alt='Logout' className={classes.logout} onClick={handleLogout} />
                        </div>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
          <span>Hallo Peter</span>
          <div className={classes.avatar}>P</div>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
