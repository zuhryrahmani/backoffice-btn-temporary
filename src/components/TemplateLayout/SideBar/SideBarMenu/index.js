// main
import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

// libraries
import { makeStyles } from '@material-ui/core';
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined, SettingOutlined, PieChartOutlined } from '@ant-design/icons';

// assets
import ringkasan from '../../../../assets/icons/ringkasan.svg';
import ringkasanWhite from '../../../../assets/icons/ringkasan-white.svg';
import bisnisPengaturan from '../../../../assets/icons/bisnis-pengaturan.svg';
import bisnisPengaturanWhite from '../../../../assets/icons/bisnis-pengaturan-white.svg';
import security from '../../../../assets/icons/security.svg';
import securityWhite from '../../../../assets/icons/security-white.svg';
import monitoringReport from '../../../../assets/icons/monitoring-report.svg';
import monitoringReportWhite from '../../../../assets/icons/monitoring-report-white.svg';

const { SubMenu } = Menu;

const SideBarMenu = () => {

  const useStyles = makeStyles({
    menu: {
      '& .ant-menu': {
        fontSize: 10,
        borderRight: 'none',
        color: '#374062',
        '& .ant-menu-item': {
          margin: 0,
          minHeight: 44,
          lineHeight: '44px',
          '& span': {
            marginLeft: 10
          },
          '&::after': {
            borderRight: 'none'
          },
          '&.ant-menu-item-selected': {
            backgroundColor: '#0061A7',
            color: '#fff',
            '&:hover': {
              color: '#fff',
              backgroundColor: '#0061A7',
            }
          },
          '&:hover': {
            color: '#374062',
            backgroundColor: '#F4F7FB'
          }
        },
        '& .ant-menu-submenu': {
          minHeight: 44,
          '& .ant-menu-submenu-title': {
            margin: 0,
            minHeight: 44,
            lineHeight: '44px',
            '& span': {
              marginLeft: 10
            },
            '& .ant-menu-submenu-arrow::before': {
              backgroundImage: 'linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))'
            }, 
            '& .ant-menu-submenu-arrow::after': {
              backgroundImage: 'linear-gradient(to right, rgba(0, 97, 167, 0.9), rgba(0, 97, 167, 0.9))'
            }, 
          },
          '& ul li': {
            paddingLeft: '58px !important',
            '&::after': {
              borderRight: 'none'
            },
            '&.ant-menu-item-selected': {
              backgroundColor: '#0061A7',
              color: '#fff',
            }
          },
          '&.ant-menu-submenu-open': {
            backgroundColor: '#0061A7',
            color: '#fff',
            '&.ant-menu-submenu-active .ant-menu-submenu-title:hover': {
              color: '#fff',
              backgroundColor: '#0061A7',
            },
            '&.ant-menu-submenu-selected': {
              color: '#fff'
            },
            '& .ant-menu-submenu-title': {
              '& .ant-menu-submenu-arrow::before': {
                backgroundImage: 'linear-gradient(to right, rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9))'
              }, 
              '& .ant-menu-submenu-arrow::after': {
                backgroundImage: 'linear-gradient(to right, rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9))'
              }, 
            },
          },
          '&.ant-menu-submenu-active': {
            '& .ant-menu-submenu-title': {
              '&:hover': {
                color: '#374062',
                backgroundColor: '#F4F7FB'
              }
            }
          },
          '&.ant-menu-submenu-selected': {
            color: '#374062'
          }
        }
      }
    }
  });
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();

  const [openKeys, setOpenKeys] = useState([]);
  const [selectedMenu, setSelectedMenu] = useState('1');

  const menus = [
    {
      id: 1,
      name: 'Ringkasan',
      path: '/ringkasan',
      logo: selectedMenu==='1' ? ringkasanWhite :  ringkasan
    },
    {
      idSubMenu: 'sub1',
      name: 'Bisnis Pengaturan',
      path: '/bisnis-pengaturan',
      logo: openKeys.includes('sub1') ? bisnisPengaturanWhite : bisnisPengaturan,
      submenus: [
        {
          id: 2,
          name: 'Bank Manager',
          path: '/bank-manager'
        },
        {
          id: 3,
          name: 'Biller Manager',
          path: '/biller-manager'
        },
        {
          id: 4,
          name: 'Biller Kategori',
          path: '/biller-kategori'
        },
        {
          id: 5,
          name: 'General Parameter',
          path: '/general-parameter'
        },
        {
          id: 6,
          name: 'Cabang',
          path: '/cabang'
        },
      ]
    },
    {
      idSubMenu: 'sub2',
      name: 'Security',
      path: '/security',
      logo: openKeys.includes('sub2') ? securityWhite : security,
      submenus: [
        {
          id: 7,
          name: 'Pengaturan Keamanan',
          path: '/pengaturan-keamanan'
        },
        {
          id: 8,
          name: 'Pengaturan Pengguna',
          path: '/pengaturan-pengguna'
        },
        {
          id: 9,
          name: 'Approval Pengguna',
          path: '/approval-pengguna'
        },
        {
          id: 10,
          name: 'Approval Config',
          path: '/approval-config'
        },
        {
          id: 11,
          name: 'Pengaturan Peran',
          path: '/pengaturan-peran'
        },
        {
          id: 12,
          name: 'Blacklist & Sandi',
          path: '/blacklist-sandi'
        },
      ]
    },
    {
      idSubMenu: 'sub3',
      name: 'Monitoring & Report',
      path: '/monitoring-report',
      logo: openKeys.includes('sub3') ? monitoringReportWhite : monitoringReport,
      submenus: [
        {
          id: 13,
          name: 'Statistik Transaksi',
          path: '/statistik-transaksi'
        },
        {
          id: 14,
          name: 'Aktivitas Nasabah',
          path: '/aktivitas-nasabah'
        },
        {
          id: 15,
          name: 'Aktivasi & Deaktivasi',
          path: '/aktivasi-deaktivasi'
        },
        {
          id: 16,
          name: 'Laporan Pendaftaran',
          path: '/laporan-pendaftaran'
        },
        {
          id: 17,
          name: 'Pengaturan Sandi',
          path: '/pengaturan-sandi'
        },
        {
          id: 18,
          name: 'Daftar Blokir',
          path: '/daftar-blokir'
        },
        {
          id: 19,
          name: 'Notifikasi',
          path: '/notifikasi'
        },
      ]
    },
  ];
  const rootSubmenuKeys = menus.slice(1, menus.length).map(item => item.idSubMenu);

  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  useEffect(() => {
    if(location) {
      switch(location.pathname) {
        case '/bisnis-pengaturan/bank-manager':
          setOpenKeys(['sub1']);
          setSelectedMenu('2');
          break;
        case '/bisnis-pengaturan/biller-manager':
          setOpenKeys(['sub1']);
          setSelectedMenu('3');
          break;
        case '/bisnis-pengaturan/biller-kategori':
          setOpenKeys(['sub1']);
          setSelectedMenu('4');
          break;
        case '/bisnis-pengaturan/general-parameter':
          setOpenKeys(['sub1']);
          setSelectedMenu('5');
          break;
        case '/bisnis-pengaturan/cabang':
          setOpenKeys(['sub1']);
          setSelectedMenu('6');
          break;
        case '/security/pengaturan-keamanan':
          setOpenKeys(['sub2']);
          setSelectedMenu('7');
          break;
        case '/security/pengaturan-pengguna':
          setOpenKeys(['sub2']);
          setSelectedMenu('8');
          break;
        case '/security/approval-pengguna':
          setOpenKeys(['sub2']);
          setSelectedMenu('9');
          break;
        case '/security/approval-config':
          setOpenKeys(['sub2']);
          setSelectedMenu('10');
          break;
        case '/security/pengaturan-peran':
          setOpenKeys(['sub2']);
          setSelectedMenu('11');
          break;
        case '/security/blacklist-sandi':
          setOpenKeys(['sub2']);
          setSelectedMenu('12');
          break;
        case '/monitoring-report/statistik-transaksi':
          setOpenKeys(['sub3']);
          setSelectedMenu('13');
          break;
        case '/monitoring-report/aktivitas-nasabah':
          setOpenKeys(['sub3']);
          setSelectedMenu('14');
          break;
        case '/monitoring-report/aktivasi-deaktivasi':
          setOpenKeys(['sub3']);
          setSelectedMenu('15');
          break;
        case '/monitoring-report/laporan-pendaftaran':
          setOpenKeys(['sub3']);
          setSelectedMenu('16');
          break;
        case '/monitoring-report/pengaturan-sandi':
          setOpenKeys(['sub3']);
          setSelectedMenu('17');
          break;
        case '/monitoring-report/daftar-blokir':
          setOpenKeys(['sub3']);
          setSelectedMenu('18');
          break;
        case '/monitoring-report/notifikasi':
          setOpenKeys(['sub3']);
          setSelectedMenu('19');
          break;
        default:
          setOpenKeys([]);
          setSelectedMenu('1');
      };
    };
  }, [location]);

  return (
    <div className={classes.menu}>
      <Menu
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        selectedKeys={[selectedMenu]}
        style={{ width: '100%' }}
      >
        {menus.map((menu, i) => (
          i === 0 ? (
            <Menu.Item
              key={menu.id}
              icon={<img src={menu.logo}
              alt={menu.name}
              style={{marginTop:-3}}/>}
              onClick={() => history.push(menu.path)}
            >
              {menu.name}
            </Menu.Item>
          ) : (
            <SubMenu key={menu.idSubMenu} icon={<img src={menu.logo} alt={menu.name} style={{marginTop:-3}}/>} title={menu.name}>
              {menu.submenus.map(submenu => (
                <Menu.Item
                  key={submenu.id}
                  onClick={() => history.push(`${menu.path}${submenu.path}`)}
                >
                  {submenu.name}
                </Menu.Item>
              ))}
            </SubMenu>
          )
        ))}
      </Menu>
    </div>
  );
};

SideBarMenu.propTypes = {};

SideBarMenu.defaultProps = {};

export default SideBarMenu;