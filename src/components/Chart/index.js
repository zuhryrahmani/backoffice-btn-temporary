import React from 'react';
import ReactApexChart from 'react-apexcharts';

const Chart = (props) => {

  // const series = props.series;
  const type = props.type;

  const series = type === "Mingguan" ? (
    [{
      name: 'Transaksi',
      data: [10000, 20000, 30000, 40000, 30000, 20000, 10000]
    },
    {
      name: 'Error',
      data: [80000, 60000, 50000, 40000, 30000, 20000, 10000]
    },
    {
      name: 'Anomali',
      data: [20000, 60000, 42000, 50000, 30000, 10000, 35000]
    }]
  )
  : type === "Bulanan" ? 
  (
    [{
      name: 'Transaksi',
      data: [10000, 15000, 20000, 80000]
    },
    {
      name: 'Error',
      data: [20000, 30000, 40000, 30000]
    },
    {
      name: 'Anomali',
      data: [30000, 80000, 50000, 20000]
    }]
  ) 
  : 
  (
    [{
      name: 'Transaksi',
      data: [10000, 15000, 39000, 30000, 25000, 20000, 24000, 80000, 30000,20000, 40000, 50000]
    },
    {
      name: 'Error',
      data: [20000, 35000, 43000, 62000, 70000, 50000, 45000, 30000, 20000, 10000, 40000, 62000]
    },
    {
      name: 'Anomali',
      data: [50000, 55000, 60000, 65000, 70000, 75000, 80000, 75000, 70000, 60000, 43000, 20000]
    }
    ]
  )
  ;
 
  const options = {
    chart: {
      height: 380,
      type: 'area',
      stacked:false,
      zoom: {
        enabled: false
      },
      toolbar: {
        show: false,
        autoSelected: 'pan'
      },
    },
    dataLabels: {
      enabled: false
    },
    markers: {
      size: 10,
    },
    stroke: {
      curve: 'smooth'
    },
    xaxis: {
      show:true,
      tooltip: {
        enabled: true,
        offsetY: 5,
        style: {
          fontSize: 12,
          fontFamily: 'FuturaMdBT',
        },
    },
      // type: 'datetime',
      categories:
        type === "Mingguan" ? 
        (
          [["Senin", "02/06/2021"],
          ["Selasa", "03/06/2021"],
          ["Rabu", "03/06/2021"],
          ["Kamis", "04/06/2021"],
          ["Jumat","05/06/2021"],
          ["Sabtu", "06/06/2021"],
          ["Minggu", "07/06/2021"]]
        ) 
        : type === "Bulanan" ? 
        (
          [["Week 1", "Jul 2021"],
          ["Week 2", "Jul 2021"],
          ["Week 3", "Jul 2021"],
          ["Week 4", "Jul 2021"]]
        ) 
        : 
        (
          [["Jan", "2021"],
          ["Feb", "2021"],
          ["Mar", "2021"],
          ["Apr", "2021"],
          ["Mei", "2021"],
          ["Jun", "2021"],
          ["Jul", "2021"],
          ["Agu", "2021"],
          ["Sep", "2021"],
          ["Okt", "2021"],
          ["Nov", "2021"],
          ["Des", "2021"]]
        ),

      // dataLabels: {
      //   enabled: false,
      //   enabledOnSeries: [2]
      // },
      
      labels: {
        show: true,
        align: 'right',
        // minWidth: 0,
        // maxWidth: 160,
        style: {
            colors: ['#374062','#374062','#374062','#374062','#374062','#374062','#374062'],
            fontSize: '13px',
            fontFamily: 'FuturaBkBT',
            fontWeight: 400,
            cssClass: 'apexcharts-yaxis-label',
        },
        // offsetY: 0,
      },
      // tickAmount: 10,
      tickPlacement: 'between'
      // tickPlacement: 'on',
    },
    yaxis: {
      type: 'numeric',
      opposite: false,
      tickAmount: 8,
      min:0,
      max:80000,
      labels: {
        show: true,
        // align: 'right',
        // minWidth: 0,
        // maxWidth: 160,
        style: {
            colors: ['#7B87AF'],
            fontSize: '10px',
            fontFamily: 'FuturaBkBT',
            fontWeight: 400,
            // cssClass: 'apexcharts-yaxis-label',
        },
        offsetY: 0,
        
    },
      
    },
    legend: {
      show: false
    },

    tooltip: {
      enabled: true,
      shared:true,
    },

    fixed: {
      enabled: true,
      position: 'topLeft',
      offsetX: 60,
      offsetY: 50,
  },

    onDatasetHover: {
      highlightDataSeries: false,
  },
    grid: {
      show: false,
      row: {
        colors: ["#F6F7FF"],
        opacity: 1,
      },
    },
    markers: {
      size: 4,
      colors: ['#75D37F', '#FF6F6F', '#FFA24B'],
      strokeColors: "#fff",
      strokeWidth: 2,
      hover: {
        size: 7,
      }
    },
    colors: ['#75D37F', '#FF6F6F', '#FFA24B'],
    selection: 'one_year',
  };

  return (
    <div>
      <ReactApexChart options={options} series={series} type="line" height={325} />
    </div>
  );

};

export default Chart;