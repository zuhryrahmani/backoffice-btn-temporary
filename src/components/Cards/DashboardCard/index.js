// main
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles, Fade } from '@material-ui/core';

// assets
import arrowDown from '../../../assets/icons/arrow-square-down.svg';
import arrowUp from '../../../assets/icons/arrow-square-up.svg';
import tachometer from '../../../assets/icons/tachometer-alt.svg';
import blue from '../../../assets/images/card-backdrop-blue.svg';
import green from '../../../assets/images/card-backdrop-green.svg';
import red from '../../../assets/images/card-backdrop-red.svg';
import purple from '../../../assets/images/card-backdrop-purple.svg';
import info from '../../../assets/icons/info.svg';

const DashboardCard = (props) => {

  const { title, number1, number2, status, percentage, color, showInfo } = props;
  const useStyles = makeStyles({
    card: {
      width: 240,
      height: 112,
      backgroundColor: '#fff',
      borderRadius: 8,
      display: 'flex',
      overflow: 'hidden'
    },
    left: {
      width: 48,
      background: `url(${color==='blue' ? blue : color==='red' ? red : color==='green' ? green : purple}) no-repeat`,
      backgroundSize: 'cover',
      backgroundPosition: 'left',
      textAlign: 'center'
    },
    right: {
      flex: 'auto',
      padding: '4px 0 4px 15px',
      fontSize: 16,
      position: 'relative'
    },
    number1: {
      fontFamily: 'FuturaHvBT',
      fontWeight: 600
    },
    number2: {
      fontFamily: 'FuturaMdBT',
      fontWeight:600,
      color:'#BCC8E7',
      margin: '-5px 0 -1px'
    },
    percentage: {
      fontSize: 13,
      color: status === 0 ? '#FF6F6F' : '#75D37F',
      lineHeight: '16px'
    },
    info: {
      position: 'absolute',
      right: 5,
      bottom: 5,
    },
    infoIcon: {
      position: 'relative',
      zIndex: 1,
      transition: '0.8s',
      '&:hover': {
        opacity: 0
      }
    },
    infoCard: {
      width: 240,
      height: 107,
      backgroundColor: '#fff',
      borderRadius: 8,
      padding: '5px 10px',
      lineHeight: 1.4,
      position: 'absolute',
      right: -5,
      bottom: -5,
    }
  });
  const classes = useStyles();

  const [infoState, setInfoState] = useState(false);

  return (
    <div className={classes.card}>
      <div className={classes.left}>
        <img src={tachometer} alt='tachometer' style={{marginTop:15}} /> 
      </div>
      <div className={classes.right}>
        <div style={{marginBottom:-2}}>{title}</div>
        <div className={classes.number1}>{number1}</div>
        <div className={classes.number2}>{number2}</div>
        <div className={classes.percentage}>
          <img src={status===0 ? arrowDown : arrowUp} alt={status===0 ? 'Arrow Up' : 'Arrow Down'} style={{marginTop:-2}} />
          <span style={{marginLeft:4}}>{percentage}</span>
        </div>
        {showInfo && (
          <div className={classes.info}>
            <img src={info} alt='info' onMouseOver={() => setInfoState(true)} onMouseOut={() => setInfoState(false)} className={classes.infoIcon} />
            <Fade in={infoState} timeout={800}>
              <div className={classes.infoCard}>
                <p style={{fontWeight:900, fontFamily:'FuturaHvBT'}}>Disclaimer</p>
                <p style={{fontSize:14}}>Proyeksi pendapatan di dapat dari admin fee dari tiap-tiap transaksi yang terdebet langsung oleh sistem BTN</p>
              </div>
            </Fade>
          </div>
        )}
      </div>
    </div>
  );
};

DashboardCard.propTypes = {
  title: PropTypes.string,
  number1: PropTypes.number,
  number2: PropTypes.number,
  status: PropTypes.number,
  percentage: PropTypes.number,
  color: PropTypes.string,
  showInfo: PropTypes.bool
};

DashboardCard.defaultProps = {
  title: 'Title',
  number1: 0,
  number2: 0,
  status: 0,
  percentage: 0,
  color: 'blue',
  showInfo: false
};

export default DashboardCard;