// main
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// libraries
import {
  TableContainer, Table, TableHead, TableRow,
  TableCell, TableBody, Button,
  Paper, Typography, makeStyles,
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

// components
import ActivityDetails from '../../Popups/ActivityDetail';

// assets
import chevronLeft from '../../../assets/icons/chevron-left-white.svg';
import chevronsLeft from '../../../assets/icons/chevrons-left-white.svg';
import chevronRight from '../../../assets/icons/chevron-right-white.svg';
import chevronsRight from '../../../assets/icons/chevrons-right-white.svg';

const useStyles = makeStyles({
  paperTable: {
    borderRadius: 8,
    border: '1px solid #BCC8E7',
    overflow: 'hidden',
    boxShadow: 'none'
  },
  tableRow: {
    "&:hover": {
      cursor: 'pointer'
    },
    '&:hover td span': {
      fontWeight: 500
    }
  },
  paginationContainer: {
    display: 'flex',
    marginTop: 24,
    justifyContent: 'space-between'
  },
  pagination: {
    height: 40,
    display: 'inline-block',
    '& ul': {
      height: 40,
      '& li': {
        height: 40,
        width: 40,
        margin: '0 5px',
        '& button': {
          fontFamily: 'FuturaMdBT',
          height: 40,
          width: 40,
          backgroundColor: '#fff',
          borderRadius: 8,
          margin: 0,
          color: '#0061A7',
          'Mui-selected': {
            backgroundColor: '#0061A7'
          }
        },
        '& div': {
          marginTop: 15
        }
      }
    }
  },
  buttonPagination: {
    minWidth: 40,
    height: 40,
    padding: 6,
    borderRadius: 8
  }
});

const TableComponent = (props) => {

  const { cols, data, pagination, page, setPage, totalPages, totalData } = props;
  const classes = useStyles();

  const rowsPerPage = 10;
  const emptyRows = rowsPerPage - data.length;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const clickFirstPage = () => {
    setPage(1);
  };
  const clickPrevPage = () => {
    if(page > 1) {
      setPage(page - 1);
    };
  };
  const clickNextPage = () => {
    if(page < totalPages) {
      setPage(page + 1);
    };
  };
  const clickLastPage = () => {
    setPage(totalPages);
  };

  const createEmptyRow = () => {
    const row = [];
    for (let i = 0; i < emptyRows; i++) {
      row.push(
        <TableRow >
          <TableCell
            colSpan={8}
            style={{
              height: 46,
              borderBottom: 'none'
            }}
          />
        </TableRow>);
    }
    return row;
  };

  const [modal, setModal] = useState(false);

  return (
    <div>
      <Paper className={classes.paperTable}>
        <TableContainer>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                {
                  cols.map((headerItem, index) => (
                    <TableCell
                      align={headerItem.headerAlign}
                      style={{
                        height: 36,
                        fontWeight: 900,
                        fontSize: 13,
                        fontFamily: "FuturaHvBT",
                        lineHeight: "18px",
                        border: "none",
                        backgroundColor: '#F4F7FB',
                        color: '#7B87AF'
                      }}
                    >
                      {headerItem.title}
                    </TableCell>
                  ))
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => {
              return (
                <TableRow key={row.id} className={classes.tableRow} onClick={() => setModal(true)}>
                  {cols.map((col, key) => (
                    <TableCell
                      align={col.align}
                      key={key}
                      wihdth={col.width}
                      style={{
                        fontSize: 13,
                        fontFamily: 'FuturaBkBT',
                        height: 46,
                        fontWeight: 600,
                        border: 'none'
                      }}
                    >{col.render(row)}</TableCell>
                  ))}
                </TableRow>
              )
                 })}
              {createEmptyRow()}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      {pagination && totalPages !== 0 &&
        <div className={classes.paginationContainer}>
          <div style={{width:'fit-content'}}>
            <Typography style={{
              color: '#7B87AF',
              fontFamily: 'FuturaBkBT',
              fontSize: 13,
              lineHeight: '40px'
            }}>
              Menampilkan 1 - {data.length} dari {totalData}
            </Typography>
          </div>
          <div style={{width:'fit-content'}}>
            <Button
              color='primary'
              variant='contained'
              disableElevation
              disabled={page===1 ? true : false}
              onClick={clickFirstPage}
              className={classes.buttonPagination}
              style={{marginRight:10}}
            >
              <img src={chevronsLeft} />
            </Button>
            <Button
              color='primary'
              variant='contained'
              disableElevation
              disabled={page===1 ? true : false}
              onClick={clickPrevPage}
              className={classes.buttonPagination}
              style={{marginRight:5}}  
            >
              <img src={chevronLeft} />
            </Button>
            <Pagination
              count={totalPages}
              page={page}
              onChange={handleChangePage}
              className={`hookPagination ${classes.pagination}`}
              color='primary'
              shape='rounded'
              hideNextButton
              hidePrevButton
            />
            <Button
              color='primary'
              variant='contained'
              disableElevation
              disabled={page===totalPages ? true : false}
              onClick={clickNextPage}
              className={classes.buttonPagination}
              style={{marginLeft:5}}
            >
              <img src={chevronRight} />
            </Button>
            <Button
              color='primary'
              variant='contained'
              disableElevation
              disabled={page===totalPages ? true : false}
              onClick={clickLastPage}
              className={classes.buttonPagination}
              style={{marginLeft:10}}
            >
              <img src={chevronsRight} />
            </Button>
          </div>
        </div>
      }
      <ActivityDetails
        isOpen={modal}
        handleClose={() => setModal(false)}
      />
    </div>
  );
};

TableComponent.propTypes = {
  cols: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  showing: PropTypes.string.isRequired,
  pagination: PropTypes.bool,
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  totalPages: PropTypes.number,
  totalData: PropTypes.number
};

TableComponent.defaultProps = {
  cols: [],
  data: [],
  pagination: true,
  page: 1,
  totalPages: 0,
  totalData: 0
};

export default TableComponent;
