// main
import React from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles } from '@material-ui/core';
import { DatePicker } from 'antd';

// assets
import calendar from '../../../assets/icons/calendar.svg';

const GeneralDatePicker = (props) => {
  
  const { value, onChange, style, placeholder } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 170,
      borderRadius: 6,
      border: '1px solid #0061A7',
      '&:hover': {
        boxShadow: '0 0 0 2px rgb(87 168 233 / 20%)',
        border: '1px solid #0061A7'
      },
      '& .ant-picker-input': {
        cursor: 'pointer',
        '& input': {
          color: '#0061A7',
          '&::placeholder': {
            color: '#0061A7',
          },
        },
        '& .ant-picker-suffix': {
          cursor: 'pointer',
        },
        '& .ant-picker-clear': {
          width: 21,
          textAlign: 'center',
          lineHeight: '22px'
        }
      }
    },
    dropdown: {
      zIndex: 1300
    }
  });
  const classes = useStyles();

  return(
    <DatePicker
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      className={classes.datePicker}
      style={style}
      suffixIcon={<img src={calendar} style={{cursor:'pointer'}} />}
      dropdownClassName={classes.dropdown}
    />
  );
};

GeneralDatePicker.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string
};

GeneralDatePicker.defaultProps = {
  value: null,
  onChange: () => console.log('click onChange'),
  style: null,
  placeholder: 'Placeholder'
};

export default GeneralDatePicker;