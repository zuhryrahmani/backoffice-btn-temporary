// main
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles, Fade } from '@material-ui/core';
import { DatePicker } from 'antd';

// assets
import calendar from '../../../assets/icons/calendar.svg';

const { RangePicker } = DatePicker;

const RangeDatePicker = (props) => {
  
  const { value, onChange, style, placeholder, datePickerID, openCalendar, setOpenCalendar, placeholderState, setPlaceholderState } = props;
  const useStyles = makeStyles({
    datePicker: {
      height: 40,
      width: 250,
      borderRadius: 6,
      border: '1px solid #0061A7',
      transform: 'translateY(-40%)',
      cursor: 'pointer',
      transition: '0.25s',
      '&:hover': {
        boxShadow: '0 0 0 2px rgb(87 168 233 / 20%)',
        border: '1px solid #0061A7'
      },
      '& .ant-picker-input': {
        '& input': {
          color: '#0061A7',
          '&::placeholder': {
            color: '#0061A7',
          },
        },
      },
      '& .ant-picker-suffix': {
        cursor: 'pointer',
      },
      '& .ant-picker-clear': {
        width: 21,
        textAlign: 'center',
        lineHeight: '22px'
      }
    },
    dropdown: {
      zIndex: 1300,
      '& .ant-picker-range-wrapper': {
        '& .ant-picker-panel-container': {
          borderRadius: 12,
          padding: 10,
          '& .ant-picker-panels': {
            '& .ant-picker-panel': {
              '&:first-child': {
                '& .ant-picker-date-panel': {
                  '& .ant-picker-header': {
                    color: '#374062',
                    '& .ant-picker-header-view': {
                      fontFamily: 'Futura',
                      fontSize: 16
                    },
                    '& .ant-picker-header-next-btn': {
                      visibility: 'visible !important'
                    },
                    '& .ant-picker-header-super-next-btn': {
                      visibility: 'visible !important'
                    }
                  },
                  '& .ant-picker-body': {
                    '& .ant-picker-content': {
                      '& thead': {
                        '& tr th': {
                          color: '#374062'
                        }
                      },
                      '& tbody': {
                        '& tr': {
                          '& td': {
                            position: 'relative',
                            '&.ant-picker-cell-today': {
                              '& .ant-picker-cell-inner::before': {
                                border: 'none'
                              }
                            },
                            '&.ant-picker-cell-range-start': {
                              '& .ant-picker-cell-inner': {
                                borderRadius: '50%',
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)',
                                lineHeight: '28px',
                                minWidth: 28,
                                height: 28,
                                color: '#F2F5FD'
                              },
                              '&::before': {
                                backgroundColor: '#E6EAF3'
                              },
                              '&.ant-picker-cell-range-start-single::before': {
                                backgroundColor: 'transparent'
                              }
                            },
                            '&.ant-picker-cell-range-end': {
                              '& .ant-picker-cell-inner': {
                                borderRadius: '50%',
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)',
                                lineHeight: '28px',
                                minWidth: 28,
                                height: 28,
                                color: '#F2F5FD'
                              },
                              '&::before': {
                                backgroundColor: '#E6EAF3'
                              }
                            },
                            '&.ant-picker-cell-in-range': {
                              '&::before': {
                                backgroundColor: '#E6EAF3'
                              }
                            },
                            '&.ant-picker-cell-in-view': {
                              '& .ant-picker-cell-inner': {
                                color: '#06377B'
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              },
              '&:last-child': {
                display: 'none'
              }
            }
          }
        }
      }
    },
    placeholder: {
      fontSize: 15,
      color: '#0061A7',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      zIndex: 1,
      lineHeight: '40px',
      borderRadius: 6,
      textAlign: 'center',
      cursor: 'pointer',
      display: 'flex',
      border: '1px solid #0061A7',
      overflow: 'hidden'
    },
    container: {
      width: 'fit-content',
      height: 40,
      position: 'relative'
    }
  });
  const classes = useStyles();

  // const [placeholderState, setPlaceholderState] = useState(true);
  // const [openCalendar, setOpenCalendar] = useState(false);
  // const [clickEl1, setClickEl1] = useState(null);
  // const [clickEl2, setClickEl2] = useState(null);
  // console.log('LOOK OPEN', openCalendar);

  // const clickOpen = e => {
  //   // console.log('LOOK EL', e.target.parentElement.nextSibling.getElementsByClassName('ant-picker-input')[0].firstElementChild)
  //   e.target.parentElement.nextSibling.getElementsByClassName('ant-picker-input')[0].firstElementChild.focus();
  //   setPlaceholderState({...placeholderState, [`placeholderState${datePickerID}`]: false});
  //   setOpenCalendar(datePickerID);
  // };

  // console.log('LOOK CLICK OPEN', clickEl1);
  // console.log('LOOK CLICK WINDOW', clickEl2);
  // console.log('LOOK VALUE IN', value);

  // useEffect(() => {
  //   if(value) {
  //     setOpenCalendar(null);
  //   } else {
  //     setPlaceholderState({...placeholderState, [`placeholderState${datePickerID}`]: true});
  //   }
  // }, [value]);

  // window.addEventListener('click', item => {
  //   const dropdownEl = document.getElementsByClassName('ant-picker-dropdown-range');
  //   let windowEl = item.target;
  //   console.log('LOOK WINDOW', windowEl);
  //   console.log('LOOK DROPDOWN', dropdownEl);
  // });

  // window.onclick = e => {
    
  //   const target = e.target;
  //   const placeholder = document.getElementsByClassName(`placeholder${datePickerID}`)[0];
  //   const dropdown = document.getElementsByClassName(`rangeDropdown${datePickerID}`)[0];
  //   const picker = document.getElementsByClassName(`picker${datePickerID}`)[0];
  //   console.log('LOOK NUM', datePickerID);
  //   console.log('LOOK PLACEHOLDER', placeholder);
  //   console.log('LOOK DROPDOWN', dropdown);
  //   console.log('LOOK picker', picker);
  //   // if(dropdown) {
  //   //   if(dropdown.contains(target) || picker.contains(target) || placeholder.contains(target)) {
  //   //     console.log('LOOK DIDALAM');
  //   //     // setOpenCalendar(datePickerID);
  //   //   } else {
  //   //     console.log('LOOK DILUAR');
  //   //     // setOpenCalendar(null);
  //   //   }
  //   //   // if(!dropdown.contains(target) || !picker.contains(target) || !placeholder.contains(target)) {
  //   //   //   setOpenCalendar(true);
  //   //   // } else {
  //   //   //   console.log('LOOK DILUAR');
  //   //   //   setOpenCalendar(false);
  //   //   // };
  //   // };
  // };

  return(
    <div className={classes.container}>
      {/* <Fade in={placeholderState && placeholderState[`placeholderState${datePickerID}`]} timeout={300}>
        <div className={`placeholder${datePickerID} ${classes.placeholder}`} onClick={clickOpen}>
          <div style={{backgroundColor: '#fff', flex:'auto'}}>{placeholder}</div>
          <div style={{width:35}}></div>
        </div>
      </Fade> */}
      <RangePicker
        value={value}
        onChange={onChange}
        className={`picker${datePickerID} ${classes.datePicker}`}
        style={style}
        suffixIcon={<img src={calendar} style={{cursor:'pointer'}} />}
        dropdownClassName={`rangeDropdown${datePickerID} ${classes.dropdown}`}
        placeholder={placeholder}
        format='DD/MM/YYYY'
        // open={openCalendar===datePickerID}
      />
    </div>
  );
};

RangeDatePicker.propTypes = {
  value: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  datePickerID: PropTypes.number.isRequired
};

RangeDatePicker.defaultProps = {
  value: null,
  onChange: () => console.log('onChange'),
  style: null,
  placeholder: ['Start', 'End'],
  datePickerID: 0
};

export default RangeDatePicker;