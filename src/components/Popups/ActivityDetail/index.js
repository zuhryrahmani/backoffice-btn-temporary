import React, { useState } from 'react';
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import {Box, Typography} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import './index.css';

import GeneralButton from "../../Button/GeneralButton";

const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& ::selection': {
        background: '#137EE1'
      }
  },
  buttonCancel: {
    color: '#E31C23',
    fontFamily:'FuturaMdBT',
    fontSize:'13px',
    textTransform: 'none',
    borderRadius: '6px',
  },
  button: {
    fontFamily:'FuturaMdBT',
    backgroundColor: '#374062',
  },
  titleModal: {
    fontFamily:'Futura',
    color:'#374062',
    fontWeight: 700,
    fontSize: '20px',
    lineHeight:'23.97px',
  },
  close: {
    textAlign: 'right',
    cursor: 'pointer'
  },
  paper: {
    width: '610px',
    height: 'auto',
    backgroundColor: 'white',
    border: '1px solid #BCC8E7',
    padding: '30px',
    alignItems: 'center',
    borderRadius: 8,
    position: 'relative'
  },
  subtitle:{
    width:100,
    height:16,
  },
  subtitle1: {
    fontFamily: 'FuturaHvBT'
  },
  logo:{
    display:'block',
    marginLeft:'auto',
    marginRight:'auto',
    top:'5.56%',
  },
  status: {
    width: 80,
    height: 23,
    background: '#FFE4E4',
    fontSize: 13,
    border: '1px solid #FF6F6F',
    borderRadius: 8,
    color: '#FF6F6F',
    fontWeight: 600,
    textAlign: 'center',
    position: 'absolute',
    top: 20,
    right: 20
  },
  tbody: {
    '& tr:hover': {
      backgroundColor: '#F4F7FB'
    }
  }
}));

// eslint-disable-next-line react/prop-types
const PopupActivityDetail = ({isOpen, handleClose}) => {
  const classes = useStyles();
  const [status, setStatus] = useState(0);

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <div
              className={classes.status}
              style={{
                color: status===0 ? '#FF6F6F' : status===1 ? '#75D37F' : '#FFA24B',
                border: `1px solid ${status===0 ? '#FF6F6F' : status===1 ? '#75D37F' : '#FFA24B'}`,
                background: status===0 ? '#FFE4E4' : status===1 ? '#E7FFDC' : '#FFF2E3'
              }}
            >
              {status===0 ? 'Eror' : status===1 ? 'Transaksi' : 'Anomali'}
            </div>
            <div>
              <Typography className={classes.titleModal}>Detail Aktivitas</Typography>
            </div>
            <Box display="flex" justifyContent="flex-start"  mt={3}>
              <Box style={{
                  marginRight:'20px'
              }}>
                <Typography variant='subtitle2' 
                className={classes.subtitle}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Nama :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 Jowo Darwono
                </Typography>
              </Box>
              <Box style={{
                  marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Tipe Nasabah :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 Bank
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,

                }}>
                 ID Nasabah :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 210001929218
                </Typography>
              </Box>
            </Box>
            <Box display="flex" justifyContent="flex-start"  mt={3}>
              <Box style={{
                marginRight:'20px',
                }}>
                <Typography variant='subtitle2' style={{
                color: '#374062',
                fontSize: '13px',
                lineHeight:'16px',
                width:100,
                height:16,

                }}>
                ID Transaksi :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                color: '#374062',
                fontSize: '13px',
                lineHeight:'16px',
                width:100,
                fontWeight:900,
                height:16,
                }}>
                21000192
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' 
                className={classes.subtitle}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Ref Number :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 210001929
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Channel :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 Mobile Apps
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,

                }}>
                 Activity :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 Transfer Other
                </Typography>
              </Box>
            </Box>
            <Box display="flex" justifyContent="flex-start"  mt={3}>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' 
                className={classes.subtitle}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Device Unique Id :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 ****21cc
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 OS :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 iOS 10
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,

                }}>
                 IP Address :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 21.0199.1821
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,
                }}>
                 Phone Type :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 iPhone X
                </Typography>
              </Box>
              <Box style={{
                marginRight:'20px',
              }}>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  height:16,

                }}>
                 Phone Brand :
                </Typography>
                <Typography variant='subtitle2' 
                className={classes.subtitle1}
                style={{
                  color: '#374062',
                  fontSize: '13px',
                  lineHeight:'16px',
                  width:100,
                  fontWeight:900,
                  height:16,
                }}>
                 iPhone
                </Typography>
              </Box>
            </Box>
            <Box>
              <Typography variant='subtitle2' style={{
                  color: '#AEB3C6',
                  fontSize: '12px',
                  textAlign:'left',
                  lineHeight:'12px',
                  marginTop:'22px',
                  fontWeight:'Bold',
                  fontFamily:'Futura'
                }}>
                  Log Aktivitas
                </Typography>
                <hr style ={{
                  border: '1px solid #AEB3C6',
                  flex:'none',
                  width:'560px',
                }}>
                </hr>
               <Box display="inline-flex" justifyContent="space-between"  mt={3} style={{width:'100%'}}>
                  
                <hr style={{
                  border: '2px solid #E6EAF3',
                  background:'#E6EAF3',
                  // width:'2px',
                  marginLeft:'11px',
                  height:'210px',
                  marginRight:'24px'
                }}>
                    
                </hr>
                 {/* {tableDummy.map((item) => ( */}
                <table
                  className={classes.td}
                  style={{
                    borderCollapse:'collapse',
                    borderSpacing:'10px 10px',
                    width: '100%'
                  }}
                
                >
                  <tbody className={classes.tbody}>
                    <tr 
                    style={{
                      width:'525px',
                      height:'26px',
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Login</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>-</td>
                    </tr>
                    <tr style={{
                      width:'525px',
                      height:'26px',
                      marginTop:'15px'
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Transaksi Keluar</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>Sistem Down</td>
                    </tr>
                    <tr style={{
                      width:'525px',
                      height:'26px',
                      marginTop:'15px'
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Cek Saldo</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>-</td>
                    </tr>
                    <tr style={{
                      width:'525px',
                      height:'26px',
                      marginTop:'15px'
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Transaksi Keluar</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>Sistem Down</td>
                    </tr>
                    <tr style={{
                      width:'525px',
                      height:'26px',
                      marginTop:'15px'
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Transaksi Keluar</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>Sistem Down</td>
                    </tr>
                    <tr style={{
                      width:'525px',
                      height:'26px',
                      marginTop:'15px'
                    }}>
                      <td style={{
                        paddingRight:'5px'
                      }}>Logout</td>
                      <td style={{
                        paddingLeft:'50px',
                        fontFamily: 'FuturaMdBT'
                      }}>12-06-2021 | 22:01:23</td>
                      <td style={{
                        paddingLeft:'20px'
                      }}>-</td>
                    </tr>
                  </tbody>
                </table>
                {/* ))} */}

               </Box>
               <hr style ={{
                  border: '1px solid #AEB3C6',
                  flex:'none',
                  width:'560px',
                }}>
                </hr>
              </Box>
            <Box display="flex" justifyContent="flex-end" mt={3}>
              <Box>
                <GeneralButton
                  label="Tutup"
                  width='77px'
                  height='40px'
                  variant='contained'
                  onClick={handleClose}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupActivityDetail.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default PopupActivityDetail;
