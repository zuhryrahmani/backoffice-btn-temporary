import React, { useState } from 'react';
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import {Box, Typography} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Timer from "react-compound-timer"

import GeneralButton from "../../Button/GeneralButton";
import logo from '../../../assets/images/exclamation-triangle.png';

const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& ::selection': {
        background: '#137EE1'
      }
  },
  titleModal: {
    marginTop:24.44,
    fontFamily:'FuturaHvBT',
    textAlign: 'center',
    color:'#374062',
    fontWeight:'900px',
    fontSize: '20px',
    lineHeight:'23.97px',
  },
  close: {
    textAlign: 'right',
    cursor: 'pointer'
  },
  paper: {
    width: '410px',
    height: 'auto',
    backgroundColor: 'white',
    border: 'none',
    padding: '30px',
    alignItems: 'center',
    borderRadius: '6px',
  },
  logo:{
    display:'block',
    marginLeft:'auto',
    marginRight:'auto',
    top:'5.56%',
  }
}));

// eslint-disable-next-line react/prop-types
const PopupUpWrongPass = ({isOpen, handleClose, title}) => {

  const classes = useStyles();
  const [disable, setDisable] = useState(true);

  const clickTutup = () => {
    setDisable(true);
    handleClose();
  };

  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <img src={logo} alt="logo" className={classes.logo}/>
            <div>
              <Typography className={classes.titleModal}>Akun Anda Ditangguhkan</Typography>
            </div>
            <Box display="flex" justifyContent="flex-start"  mt={3}>
              <Box>
                <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  textAlign:'center',
                  lineHeight:'16px',
                  marginBottom:20,
                }}>
                 Akun anda saat ini sedang ditangguhkan dikarenakan salah memasukan password sebanyak 3x
                </Typography>
              </Box>
            </Box>
            <Box>
            <Typography variant='subtitle2' style={{
                color: '#374062',
                fontSize: '15px',
                textAlign:'center',
                lineHeight:'18px',
                letterSpacing:'0.01em',
                fontWeight:'Bold',
                fontFamily: 'Futura'
            }}>
                {
                  <Timer
                    initialTime={60000}
                    startImmediately={true}
                    direction="backward"
                    checkpoints={[
                      {
                        time: 1,
                        callback: () => setDisable(false),
                      }
                    ]}
                  >
                    
                    {() => (
                        <React.Fragment>
                            <Timer.Minutes /> :{' '}
                            <Timer.Seconds /> :{' '}
                            <Timer.Milliseconds />
                        </React.Fragment>
                    )}
                  </Timer>
                }
            </Typography>
            </Box>
            <Box>
              <Typography variant='subtitle2' style={{
                  color: '#374062',
                  fontSize: '13px',
                  textAlign:'center',
                  lineHeight:'16px',
                  marginTop:'20px',
                }}>
                  Silahkan coba kembali setelah timer diatas selesai melakukan hitungan mundur
                </Typography>
              </Box>
            <Box display="flex" justifyContent="space-around" mt={3}>
              <Box>
                <GeneralButton
                  label="Tutup"
                  width='357px'
                  height='40px'
                  variant='contained'
                  onClick={clickTutup}
                  disabled={disable}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupUpWrongPass.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default PopupUpWrongPass;
