import React, {useState, useRef, useEffect} from 'react';
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import {Box, Typography} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Timer from "react-compound-timer"

import GeneralButton from "../../Button/GeneralButton";
import ButtonOutlined from "../../Button/ButtonOutlined";
import logo from '../../../assets/images/exclamation-triangle.png';


const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& ::selection': {
      background: '#137EE1'
    }
  },
  titleModal: {
    marginTop:24.44,
    fontFamily:'FuturaHvBT',
    textAlign: 'center',
    color:'#374062',
    fontWeight:'900px',
    fontSize: '20px',
    lineHeight:'23.97px',
  },
  paper: {
    width: '320px',
    height: 'auto',
    backgroundColor: 'white',
    border: 'none',
    padding: '30px 26px',
    alignItems: 'center',
    borderRadius: '6px',
  },
  logo:{
    display:'block',
    marginLeft:'auto',
    marginRight:'auto',
    top:'5.56%',
  }
}));

// eslint-disable-next-line react/prop-types
const PopupUpSessionTime = ({isOpen, onCancel, onContinue}) => {
  const classes = useStyles();
  const [timer, setTimer] = useState('00:00:00');
  return (
    <div>
      <Modal
        className={classes.modal}
        open={isOpen}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={isOpen}>
          <div className={classes.paper}>
            <img src={logo} alt="logo" className={classes.logo}/>
            <div>
              <Typography className={classes.titleModal}>Session Timeout</Typography>
            </div>
            <Box display="flex" justifyContent="flex-start"  mt={3} style={{marginTop:10}}>
              <Box>
                <Typography style={{
                  color: '#374062',
                  fontSize: '13px',
                  textAlign:'center',
                  lineHeight:'16px',
                  marginBottom:20,
                }}>
                  Sisa waktu anda untuk sesi ini sudah berakhir, apakah anda ingin melanjutkan sesi ini ?
                </Typography>
              </Box>
            </Box>
            <Box>
              <Typography style={{
                  color: '#374062',
                  fontSize: '13px',
                  textAlign:'center',
                  lineHeight:'16px',
                }}>
                  Silahkan pilih dalam <b>{
                    <Timer
                    initialTime={60000}
                    direction="backward"
                >
                    {() => (
                        <React.Fragment>
                            <Timer.Minutes />:
                            <Timer.Seconds />
                        </React.Fragment>
                    )}
                </Timer>
                    }</b> sebelum sistem melakukan auto logout
                </Typography>
              </Box>
            <Box display="flex" justifyContent="flex-start"  mt={5}>
              <Box flexGrow={1}>
                <ButtonOutlined
                  label="Batal"
                  width='76px'
                  height='40px'
                  variant='contained'
                  onClick={onCancel}
                />
              </Box>
              <Box>
                <GeneralButton
                  label="Lanjutkan"
                  width='108px'
                  height='40px'
                  variant='contained'
                  onClick={onContinue}
                  className={classes.Button}
                />
              </Box>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

PopupUpSessionTime.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onContinue: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default PopupUpSessionTime;
