// main
import React, {  } from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles } from '@material-ui/core';

const Title = (props) => {

  const { label, children } = props;
  const useStyles = makeStyles({
    title: {
      width: '100%',
      height: 80,
      backgroundColor: '#fff',
      boxShadow: 'inset 0px -1px 0px #E6EAF3',
      lineHeight: '80px',
      fontSize: 20,
      paddingLeft: 30,
      fontWeight: 900,
      position: 'relative',
      fontFamily: 'FuturaHvBT'
    },
    children: {
      position: 'absolute',
      right: 30,
      top: '50%',
      transform: 'translateY(-50%)',
      lineHeight: '40px',
    }
  });
  const classes = useStyles();

  return (
    <div className={classes.title}>
      {label}
      <div className={classes.children}>
        {children}
      </div>
    </div>
  );
};

Title.propTypes = {
  label: PropTypes.string
};

Title.defaultProps = {
  label: ''
};

export default Title;