// main
import React from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles, Tabs, Tab } from '@material-ui/core';

const GeneralTabs = (props) => {

  const { value, onChange, tabs, style } = props;
  const useStyles = makeStyles({
    rootTabs: {
      minHeight: 40,
      backgroundColor: '#0061A7',
      borderRadius: 8,
      height: 40,
      color: '#fff',    
      width: "fit-content",
      position: "relative",
    },
    tabsIndicator: {
      display: 'none',
    },  
    rootItemTabs: {
      minHeight: 40,
      minWidth: 72,
      width: tabs.length === 2 && '50%',
      padding: '7px 10px',
    },
    selectedTabItem: {
      backgroundColor: '#F4F7FB',
      color: '#BCC8E7',
    },
    wrapperTabItem: {
      textTransform: 'none',
      fontSize: 13
    },
  });
  const classes = useStyles();

  // tabs styles
  const tabsStyles = {
    root: classes.rootTabs,
    indicator: classes.tabsIndicator,
  };
  const tabItemStyles = {
    root: classes.rootItemTabs,
    selected: classes.selectedTabItem,
    wrapper: classes.wrapperTabItem,
  };

  return(
    <Tabs
      classes={tabsStyles}
      value={value}
      onChange={onChange}
      style={style}
      centered
    >
      {tabs.map(item => <Tab classes={tabItemStyles} label={item} />)}
    </Tabs>

  );
};

GeneralTabs.propTypes = {
  value: PropTypes.string,
  tabs: PropTypes.array,
  onChange: PropTypes.func
};

GeneralTabs.defaultProps = {
  value: '',
  tabs: [],
  onChange: () => console.log('GeneralTabs')
};

export default GeneralTabs;