// main
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// libraries
import { makeStyles } from '@material-ui/core';
import { Select } from 'antd';

// components
import TextField from '../../TextField/AntdTextField';

// assets
import arrow from '../../../assets/icons/chevron-down-white.svg';

const SearchWithDropdown = (props) => {

  const { options, dataSearch, setDataSearch } = props;
  const useStyles = makeStyles({
    search: {
      height: '40px',
      '& .ant-select': {
        width: 110,
        top: -3,
        fontFamily: 'Futura',
        "& .ant-select-selector": {
          borderRadius: "8px 0 0 8px",
          border: 'none',
          width: 110,
          fontSize: 13,
          fontWeight: 700,
          color: '#fff',
          backgroundColor: '#0061A7',
        },
        "& .ant-select-arrow": {
          width: 16,
          height: 16,
          top: "45%",
        },
        '&.ant-select-open': {
          "& .ant-select-selector": {
            // boxShadow: '0 0 0 2px rgb(87 168 223 / 20%)'
            boxShadow: 'none'
          },
          "& .ant-select-arrow": {
            top: "45%",
            transform: 'rotate(180deg)'
          },
        },
        '&.ant-select-focused': {
          "& .ant-select-selector": {
            // boxShadow: '0 0 0 2px rgb(87 168 223 / 20%)'
            boxShadow: 'none'
          },
        },
      },
      '& .ant-input-affix-wrapper': {
        boxShadow: 'none',
        '&:hover': {
          boxShadow: 'none'
        },
        '& input': {
          color: '#0061A7',
          '&::placeholder': {
            color: '#BCC8E7'
          }
        },
        '&.ant-input-affix-wrapper-focused': {
          boxShadow: 'none'
        }
      },
    },
    dropdown: {
      zIndex: 1300,
      '& .ant-select-item-option-active': {
        '&:hover': {
          backgroundColor: '#F4F7FB'
        }
      },
      '& .ant-select-item-option-selected': {
        backgroundColor: '#cfe0e6 !important'
      }
    }
  });
  const classes = useStyles();

  const [select, setSelect] = useState(options ? options[0] : '');
  const [input, setInput] = useState(null);

  const clickSearch = () => {
    setDataSearch({
      select: select,
      input: input
    });
  };

  return (
    <div className={classes.search}>
      <Select
        size='large'
        value={select}
        onChange={value => setSelect(value)}
        suffixIcon={<img src={arrow} />}
        dropdownClassName={classes.dropdown}
      >
        {options.map(item => <Option value={item}>{item}</Option>)}
      </Select>
      <TextField
        type='search'
        placeholder='pencarian...'
        value={input}
        onChange={e => setInput(e.target.value)}
        style={{
          width:190,
          borderRadius: '0 8px 8px 0',
          borderLeft: 'none',
          top:-6
        }}
        onClickSearch={clickSearch}
      />
    </div>
  );
};

SearchWithDropdown.propTypes = {
  options: PropTypes.array,
  dataSearch: PropTypes.object.isRequired,
  setDataSearch: PropTypes.func.isRequired
};

SearchWithDropdown.defaultProps = {
  options: []
};

export default SearchWithDropdown;