
import React from "react";
import Button from "@material-ui/core/Button";
import {withStyles} from "@material-ui/core/styles";
import PropTypes from 'prop-types';
import * as Colors from '../../../assets/theme/colors';
import '../../../index.css';

const styles = () => ({
  root: {
    fontFamily: "Futura",
    fontSize: 14,
    lineHeight:'15px',
    textAlign:'center',
    textTransform: 'capitalize',
    borderRadius: '8px',
    boxShadow: '0px 6px 6px 2px rgba(120, 191, 254, 0.12)',
    padding: '0 20px 3px',
    '&:hover' : {
      boxShadow: '0px 6px 6px 2px rgba(120, 191, 254, 0.12)',
    },
    '& ::selection': {
      background: '#137EE1'
    },
  },

});

const GeneralButton = ({label, classes, iconPosition, buttonIcon,onClick, width, height, style, disabled}) => {

  let button;
  if (iconPosition === 'endIcon') {
    button = <Button className={classes.root}
      endIcon={buttonIcon}
      onClick={onClick}
      disabled={disabled}
      style={{
        width,
        height,
        ...style
      }}
      color='primary'
      variant='contained'
    >{label}
    </Button>;
  } else {
    button = <Button className={classes.root}
      startIcon={buttonIcon}
      onClick={onClick}
      disabled={disabled}
      style={{
        width,
        height,
        ...style
      }}
      color='primary'
      variant='contained'
    >{label}
    </Button>;
  }
  return (
    <>
      {button}
    </>
  );
};

GeneralButton.propTypes = {
  label: PropTypes.string,
  iconPosition: PropTypes.string, // startIcon or endIcon
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  buttonIcon: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

GeneralButton.defaultProps = {
  label: "Submit",
  // eslint-disable-next-line react/default-props-match-prop-types
  buttonIcon: '',
  iconPosition: "endIcon",
  // eslint-disable-next-line react/default-props-match-prop-types
  width: '86px',
  // eslint-disable-next-line react/default-props-match-prop-types
  height: '33px',
  disabled: false
};

export default withStyles(styles)(GeneralButton);

// How to use...?
// A==> import MuiIconLabelButton
// import MuiIconLabelButton from '../../components/Button/MuiIconLabelButton';

// B==> import icon
// import { ReactComponent as AngleLeftIcon } from '../../assets/images/angle-left.svg';

// C==> set component
// <MuiIconLabelButton label="Tombol" iconPosition="startIcon" buttonIcon={<AngleLeftIcon/>}/>
