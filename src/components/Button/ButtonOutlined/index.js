
import React from "react";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from 'prop-types';

import * as Colors from '../../../assets/theme/colors';

const styles = () => ({
  root: {
    fontFamily: "Futura",
    fontSize: 14,
    height: '15px',
    padding: '0 20px 3px',
    boxShadow: "0px 6px 6px 2px rgba(120, 191, 254, 0.12)",
    fontWeight: 'bold',
    border: '1px solid #E31C23',
    backgroundColor: Colors.White,
    textTransform:'none',
    color: Colors.RedHard,
    borderRadius: 8,

  }
});

const ButtonOutlined = ({ label, classes, iconPosition, buttonIcon, onClick, width, height, className='' }) => {

  let button;
  if (iconPosition === 'endIcon') {
    button = <Button className={className ? `${className} ${classes.root}` : classes.root}
      endIcon={buttonIcon}
      onClick={onClick}
      style={{
        width,
        height
      }}
    >{label}
    </Button>;
  } else {
    button = <Button className={className ? `${className} ${classes.root}` : classes.root}
      startIcon={buttonIcon}
      onClick={onClick}
      style={{
        width,
        height
      }}
    >{label}</Button>;
  }
  return (
    <>
      {button}
    </>
  ) ;
};

ButtonOutlined.propTypes = {
  label: PropTypes.string,
  iconPosition: PropTypes.string, // startIcon or endIcon
  classes: PropTypes.object.isRequired,
  buttonIcon: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

ButtonOutlined.defaultProps  = {
  label: "Submit",
  // eslint-disable-next-line react/default-props-match-prop-types
  buttonIcon: '',
  iconPosition: "endIcon",
  // eslint-disable-next-line react/default-props-match-prop-types
  width: '116px',
  // eslint-disable-next-line react/default-props-match-prop-types
  height: '40px'
};

export default withStyles(styles)(ButtonOutlined);

