import React, { useState, useRef } from 'react';
import IdleTimer from 'react-idle-timer';
import PropTypes from 'prop-types';
import SessionTimeout from "../Popups/SessionTimeout";
import { Layout } from 'antd';

const { Content } = Layout;

function index() {
    const [modalIsOpen, setModalIsOpen] = useState(false)
    const idleTimerRef = useRef(null)
    const sessionTimeoutRef = useRef(null)

    const onIdle =() =>{
        setModalIsOpen(true)
        sessionTimeoutRef.current = setTimeout(logOut, 60000)
    }

    const stayActive = () =>{
        setModalIsOpen(false)
        clearTimeout(sessionTimeoutRef.current)
    }

    const logOut = () => {
        setModalIsOpen(false)
        sessionStorage.clear();
        window.location.replace('/');
    }

    return (
        <div>
            <SessionTimeout
                isOpen={modalIsOpen}
                onContinue={stayActive}
                onCancel={logOut}
            >
            </SessionTimeout>
            <IdleTimer 
                ref={idleTimerRef} 
                timeout={ 900000 } 
                onIdle={onIdle}>
            </IdleTimer>
        </div>
    )
}

index.propTypes = {

};
  
index.defaultProps = {

};

export default index
