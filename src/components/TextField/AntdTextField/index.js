// main
import React from 'react';
import PropTypes from 'prop-types';

// libraries
import { Input } from 'antd';
import { makeStyles } from '@material-ui/core';

// assets
import search from '../../../assets/icons/search.svg';

const AntdTextField = (props) => {

  const { placeholder, value, onChange, style, textAreaRows, type, disabled, prefix, suffix, className, onClickSearch } = props;
  const useStyles = makeStyles({
    input: {
      fontFamily: 'FuturaBkBT',
      '&:hover': {
        boxShadow: '0 0 0 2px rgb(87 168 233 / 20%)'
      },
      '&.ant-input-affix-wrapper-focused': {
        boxShadow: '0 0 0 2px rgb(87 168 233 / 20%)'
      },
      '& .ant-input': {
        boxShadow: 'none'
      },
      '&:focus': {
        boxShadow: '0 0 0 2px rgb(87 168 233 / 20%) !important'
      }
    }
  });
  const classes = useStyles();

  return(
    type==='textArea' ? (
      <Input.TextArea
        rows={textAreaRows}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        style={{
          borderRadius: 10,
          border: '1px solid #BCC8E7',
          width: 320,
          color: '#374062',
          ...style
        }}
        disabled={disabled}
        className={className}
      />
    ) : type==='password' ? (
      <Input.Password
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        style={{
          borderRadius: 10,
          border: '1px solid #BCC8E7',
          width: 320,
          height: 40,
          color: '#374062',
          ...style
        }}
        className={className ? `${className} ${classes.input}` : classes.input}
        disabled={disabled}
      />
    ) : type==='search' ? (
      <Input
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        style={{
          borderRadius: 10,
          border: '1px solid #BCC8E7',
          width: 320,
          height: 40,
          color: '#374062',
          ...style
        }}
        suffix={<img src={search} style={{cursor:'pointer'}} onClick={onClickSearch} />}
        className={className ? `${className} ${classes.input}` : classes.input}
        disabled={disabled}
      />
    ) : (
      <Input
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        style={{
          borderRadius: 10,
          border: '1px solid #BCC8E7',
          width: 320,
          height: 40,
          color: '#374062',
          ...style
        }}
        disabled={disabled}
        prefix={prefix}
        suffix={suffix}
        className={className ? `${className} ${classes.input}` : classes.input}
      />
    )
  );
};

AntdTextField.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  textAreaRows: PropTypes.number,
  disabled: PropTypes.bool,
  prefix: PropTypes.element,
  suffix: PropTypes.element,
  className: PropTypes.string,
  onClickSearch: PropTypes.func
};

AntdTextField.defaultProps = {
  placeholder: '',
  value: '',
  onChange: () => console.log('AntdTextField'),
  type: 'input',
  textAreaRows: 6,
  disabled: false,
  prefix: null,
  suffix: null,
  className: '',
  onClickSearch: () => console.log('click search')
};

export default AntdTextField;