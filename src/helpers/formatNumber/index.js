const formatNumber = (n) => {
  const num = n.toString().split('').reverse().join('');
  const len = Math.ceil(num.length/3);
  const arr = [];
  for(let i=0; i<len; i++) {
    arr.push(num.slice(i*3, (i+1)*3));
  };
  const result = arr.reverse().map(item => item.split('').reverse().join('')).join('.');
  return result;
};

export default formatNumber;