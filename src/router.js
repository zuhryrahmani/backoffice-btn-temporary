// main
import React, { useState, useEffect, createContext } from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import i18n from './helpers/i18next/i18n';
import store from './helpers/store/store';
import './index.css';

// libraries
import { Layout } from 'antd';
import { ThemeProvider } from '@material-ui/styles';

// containers
import Container from './layout';
import LoginScreen from './containers/LoginScreen';
import Ringkasan from './containers/Ringkasan';

// assets
import theme from './assets/theme/theme';

const { Header, Content } = Layout;
export const RootContext = createContext();

const AppIndex = () => {

  const location = useLocation().pathname;
  
  const [loc, setLoc] = useState('/');
  const [userName, setUserName] = useState(null);
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    if(location==='/' || location==='/login' || location==='/login/') {
      setLoc(location);
    };
  }, [location]);

  useEffect(() => {
    if(sessionStorage) {
      setUserName(sessionStorage.getItem('username'));
      setUserRole(sessionStorage.getItem('role'));
    };
  }, [sessionStorage]);

  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <RootContext.Provider
            value={{
              userName,
              userRole
            }}
          >
            <Switch>
            <Route exact path={loc} component={LoginScreen} />
              <Container isLoggedIn={userName} >
                <Content
                  style={{
                    width: '100%',
                    margin: '60px 0 0 200px',
                    paddingRight: 200
                  }}
                >
                  <Route exact path="/ringkasan" component={Ringkasan} />
                </Content>
              </Container>
            </Switch>
          </RootContext.Provider>
        </ThemeProvider>
      </Provider>
    </I18nextProvider>
  );
};

export default AppIndex;
