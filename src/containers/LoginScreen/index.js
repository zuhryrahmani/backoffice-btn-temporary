
import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Form, Layout } from 'antd';
import Grid from '@material-ui/core/Grid';

//import component
import GeneralButton from "../../components/Button/GeneralButton";
import  TextField from "../../components/TextField/AntdTextField";
import WrongPassword from "../../components/Popups/WrongPassword";

//import Logo
import bannerImage from '../../assets/images/btnlogo.png';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { auth } from "../../services/mockup_data/dataAuthenticationLogin";

// import { nominalTypeHack } from "prop-types";

const { Content } = Layout;

const LoginScreen = () =>  {

  //create 2 role (admin & user)
  //create 3 condition in user role
  //and check username & password are match before return 

  const [count, setCount] = useState(0);
  const [username, setUsername] = useState('');
  const [usernameError1, setUsernameError1] = useState(false);
  const [usernameError2, setUsernameError2] = useState(false);
  const [password, setPassword] = useState('');
  const [passwordError1, setPasswordError1] = useState(false);
  const [passwordError2, setPasswordError2] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const useStyles = makeStyles({
    container: {
      // minHeight: '100vh',
      backgroundColor: '#fff',
      display: 'flex',
      flexDirection: 'column',
      flexGrow: 1,
    },
    loginTitle: {
      fontFamily: 'Futura',
      fontWeight: 700,
      fontStyle:'normal',
      fontSize: '60px',
      color: '#374062',
      textAlign:'center',
      lineHeight:'59.68px',
      height:60,
      width:167,
      margin: '159px 0 78px'
    },
    loginSubtitle: {
      fontFamily: 'FuturaBkBT',
      lineHeight:'15.23px',
      fontSize: '13px',
      height:16,
      color: '#BCC8E7',
      margin: '10px 0px 14px',
    },
    inputTitle: {
      fontFamily: 'FuturaBkBT',
      fontWeight: 400,
      fontSize: '13px',
      color: '#374062',
      lineHeight:'15.23px',
      marginTop: 29,
      width: '320px',
      transition: '0.3s'
    },
    banner: {
      width: '583px',
      height:'135px',
      display: 'block'
    },
    paper: {
      height: 140,
      width: 100,
    },
    Item :{
      marginLeft:'90px',
      color:'#FFCCC1'
    },
    error: {
      fontSize: 13,
      color: '#FF6F6F',
      position: 'absolute',
      left: 0,
      transition: '0.3s ease-in-out'
    },
    form: {
      position:'relative',
      marginBottom:34,
      '& .ant-form-item-control-input .inputUsername': {
        '&:hover': {
          boxShadow: `0 0 0 2px rgb(${usernameError1 || usernameError2 ? '255 111 111' : '87 168 233'} / 20%)`
        },
        '&.ant-input-affix-wrapper-focused': {
          boxShadow: `0 0 0 2px rgb(${usernameError1 || usernameError2 ? '255 111 111' : '87 168 233'} / 20%)`
        },
        '& .ant-input': {
          boxShadow: 'none'
        },
        '&:focus': {
          boxShadow: `0 0 0 2px rgb(${usernameError1 || usernameError2 ? '255 111 111' : '87 168 233'} / 20%) !important`
        }
      },
      '& .ant-form-item-control-input .inputPassword': {
        '&:hover': {
          boxShadow: `0 0 0 2px rgb(${passwordError1 || passwordError2 ? '255 111 111' : '87 168 233'} / 20%)`
        },
        '&.ant-input-affix-wrapper-focused': {
          boxShadow: `0 0 0 2px rgb(${passwordError1 || passwordError2 ? '255 111 111' : '87 168 233'} / 20%)`
        },
        '& .ant-input': {
          boxShadow: 'none'
        },
        '&:focus': {
          boxShadow: `0 0 0 2px rgb(${passwordError1 || passwordError2 ? '255 111 111' : '87 168 233'} / 20%) !important`
        }
      }
    }
  });
  const classes = useStyles();

  const navigateBaseOnRole = e => {
    e.preventDefault();
    if(username && password) {
      setUsernameError2(false);
      setPasswordError2(false);
      if(username==='admin@gmail.com' || username==='admin') {
        setUsernameError1(false);
        if(password==='ist123') {
          setPasswordError1(false);
          sessionStorage.setItem('username', 'admin');
          sessionStorage.setItem('role', 1);
          localStorage.removeItem('loginCount');
          window.location.replace('/ringkasan');
        } else {
          setPasswordError1(true);
        };
      } else if(username==='user@gmail.com'|| username==='user') {
        setUsernameError1(false);
        if(password==='user123') {
          setPasswordError1(false);
          sessionStorage.setItem('username', 'user');
          sessionStorage.setItem('role', 0);
          localStorage.removeItem('loginCount');
          window.location.replace('/ringkasan');
        } else {
          setPasswordError1(true);
          setCount(count+1);
        };
      } else {
        setUsernameError1(true);
        setPasswordError1(true);
        setCount(count+1);
      };
    } else if (username && !password) {
      setUsernameError2(false);
      setPasswordError2(true);
      setUsernameError1(false);
      setPasswordError1(false);
    } else if (!username && password) {
      setUsernameError2(true);
      setPasswordError2(false);
      setUsernameError1(false);
      setPasswordError1(false);
    } else {
      setUsernameError2(true);
      setPasswordError2(true);
      setUsernameError1(false);
      setPasswordError1(false);
    };
  }

  const onTypeUsername = (e) => {
    if(e.target.value) {
      setUsernameError1(false);
      setUsernameError2(false);
    } else {
      setUsernameError1(false);
      setUsernameError2(true);
    }
    setUsername(e.target.value);
  }

  const onTypePassword = (e) => {
    if(e.target.value) {
      setPasswordError1(false);
      setPasswordError2(false);
    } else {
      setPasswordError1(false);
      setPasswordError2(true);
    }
    setPassword(e.target.value);
  };

  const handleWrongPassword = () => {
    setCount(0);
    localStorage.removeItem('loginCount');
    setModalIsOpen(false);
  };

  useEffect(() => {
    if(count) {
      localStorage.setItem('loginCount', count);
    };
    if(localStorage.getItem('loginCount')==3) {
      setModalIsOpen(true);
    };
  }, [count]);

  useEffect(() => {
    if(sessionStorage.getItem('username')) {
      window.location.replace('/ringkasan');
    };
  }, [sessionStorage]);
      
  return (
    <div className={classes.container}>
      <Grid>
        <div style={{float:'right', marginRight:'80px',marginTop:'292px'}}>
          <img className={classes.banner} alt="" src={bannerImage} />
        </div>
        <div style={{float:'left'}}>
          <Form onFinishFailed={navigateBaseOnRole} autoComplete='off' style={{marginLeft:90}}>
            <div className={classes.loginTitle}>Login</div>  
            <div className={classes.inputTitle} style={{color: usernameError1 || usernameError2 ? '#FF6F6F' : '#374062'}}>Username :</div> 
            <Form.Item className={classes.form}>
              <TextField
                type='input'
                style={{
                  margin: '5px 0',
                  border: `1px solid ${usernameError1 || usernameError2 ? '#FF6F6F' : '#BCC8E7'}`
                }}
                className='inputUsername'
                placeholder='Username Anda'
                value={username}
                onChange={onTypeUsername}
              />
              <p
                className={classes.error}
                style={{
                  opacity: usernameError1 ? 1 : 0,
                  bottom: usernameError1 ? -18 : 5
                }}
              >
                Username Anda Salah
              </p>
              <p
                className={classes.error}
                style={{
                  opacity: usernameError2 ? 1 : 0,
                  bottom: usernameError2 ? -18 : 5
                }}
              >
                Silahkan masukkan username
              </p>
            </Form.Item>
            <div className={classes.inputTitle} style={{color: passwordError1 || passwordError2 ? '#FF6F6F' : '#374062'}}>Sandi :</div>
            <Form.Item className={classes.form}>
              <TextField
                type='password'    
                style={{
                  margin: '5px 0',
                  border: `1px solid ${passwordError1 || passwordError2 ? '#FF6F6F' : '#BCC8E7'}`
                }}
                className='inputPassword'
                placeholder='Sandi Anda'
                value={password}
                onChange={onTypePassword}
              />
              <p
                className={classes.error}
                style={{
                  opacity: passwordError1 ? 1 : 0,
                  bottom: passwordError1 ? -18 : 0
                }}
              >
                Password Anda Salah
              </p>
              <p
                className={classes.error}
                style={{
                  opacity: passwordError2 ? 1 : 0,
                  bottom: passwordError2 ? -18 : 0
                }}
              >
                Silahkan masukkan password
              </p>
            </Form.Item>
            <div className={classes.loginSubtitle} >*Silahkan login menggunakan username dan password LDAP</div>
            <Form.Item>
              <button onClick={navigateBaseOnRole} style={{display:'none'}}></button>
              <GeneralButton 
                label='Masuk'
                width='95px'
                height='40px'
                variant='contained'
                iconPosition='endIcon'
                onClick= {navigateBaseOnRole}
              />
            </Form.Item>
          </Form>
        </div>
      </Grid>
      <WrongPassword
        isOpen={modalIsOpen}
        title='Akun Anda Ditangguhkan'
        handleClose={handleWrongPassword}
      />
    </div>        
  ) 
};

LoginScreen.propTypes = {

};

LoginScreen.defaultProps = {

};

export default LoginScreen;