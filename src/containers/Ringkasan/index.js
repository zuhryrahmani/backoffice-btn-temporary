// main
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { cardDummy, tableDummy } from '../../services/mockup_data/dataDashboard';

// libraries
import { makeStyles } from '@material-ui/core';

// components
import Title from '../../components/Title';
import Search from '../../components/Search/SearchWithDropdown';
import Filter from '../../components/Filter/GeneralFilter';
import Card from '../../components/Cards/DashboardCard';
import Table from '../../components/Table/GeneralTablePagination';
import Chart from '../../components/Chart';
import NoDataModal from '../../components/Popups/NoData';

// assets
import bookAccount from '../../assets/images/book-account.svg';

// helpers
import formatNumber from '../../helpers/formatNumber';
import getPercentage from '../../helpers/getPercentage';

const Ringkasan = (props) => {

  const {  } = props;
  const useStyles = makeStyles({
    ringkasan: {

    },
    center: {
      margin: '94px auto 0',
      width: 'fit-content',
      textAlign: 'center'
    },
    account: {
      marginBottom: 20
    },
    desc: {
      fontSize: 24
    },
    container: {
      padding: '20px 30px'
    },
    card: {
      display: 'flex',
      justifyContent: 'space-between',
      margin: '30px 0 20px'
    },
    chart: {
      minHeight: 314,
      backgroundColor: '#fff',
      borderRadius: 8,
      padding: '18px 15px 15px',
      marginBottom: 20,
    },
    title: {
      fontFamily: 'FuturaHvBT',
      fontSize: 16,
      fontWeight: 900
    }
  });
  const classes = useStyles();

  const [role, setRole] = useState(0);
  const [dataCard, setDataCard] = useState(null);
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(1);
  const [chartType, setChartType] = useState(0);
  // const [series, setSeries] = useState('Mingguan');
  const [noDataModal, setNoDataModal] = useState(false);
  const [dataSearch, setDataSearch] = useState(null);
  const [dataFilter, setDataFilter] = useState(null);
  console.log('LOOK SEARCH', dataSearch);
  console.log('LOOK FILTER', dataFilter);

//   const series = [{
//     name: "keterangan 1",
//     data: Array(7).fill().map((element, index) => (Math.floor(Math.random() * 10) + 1)*10000),
//   },
//   {
//     name: "keterangan 2",
//     data: Array(7).fill().map((element, index) => (Math.floor(Math.random() * 10) + 1)*10000),
//   },
//   {  
//     name: 'keterangan 3',
//     data: Array(7).fill().map((element, index) => (Math.floor(Math.random() * 10) + 1)*10000),
//   }

 
// ];

// const [type, setType]

  const tableConfig = [
    {
      title: 'Transaksi ID',
      headerAlign: 'left',
      align: 'left',
      render: rowData => {
        return <span>{rowData.id}</span>;
      },
    },
    {
      title: 'Tgl & Waktu',
      headerAlign: 'left',
      align: 'left',
      render: rowData => {
        return <span>{rowData.dateTime}</span>;
      },
    },
    {
      title: 'Aktivitas',
      headerAlign: 'left',
      align: 'left',
      render: rowData => {
        return <span>{rowData.activity}</span>;
      },
    },
    {
      title: 'Nasabah ID',
      headerAlign: 'left',
      align: 'left',
      render: rowData => {
        return <span>{rowData.userId}</span>;
      },
    },
    {
      title: 'Versi Aplikasi',
      headerAlign: 'left',
      align: 'left',
      render: rowData => {
        return <span>{rowData.version}</span>;
      },
    },
    {
      title: 'Status',
      headerAlign: 'center',
      align: 'center',
      render: rowData => {
        if (rowData.status === 0) {
          return <span style={{
            width: 80,
            height: 23,
            background: '#FFE4E4',
            fontSize: 13,
            border: '1px solid #FF6F6F',
            borderRadius: 8,
            color: '#FF6F6F',
            display: 'inline-block',
            fontWeight: 600
          }}>Eror</span>;
        } else if (rowData.status === 1) {
          return <span style={{
            width: 80,
            height: 23,
            background: '#E7FFDC',
            fontSize: 13,
            border: '1px solid #75D37F',
            borderRadius: 8,
            color: '#75D37F',
            display: 'inline-block',
            fontWeight: 600
          }}>Transaksi</span>;
        } else {
          return <span style={{
            width: 80,
            height: 23,
            background: '#FFF2E3',
            fontSize: 13,
            border: '1px solid #FFA24B',
            borderRadius: 8,
            color: '#FFA24B',
            display: 'inline-block',
            fontWeight: 600
          }}>Anomali</span>;
        }
      }
    }
  ];

  // get dataCard
  useEffect(() => {
    if(cardDummy) {
      const res = {
        totalAktivitas: {
          before: formatNumber(cardDummy.totalAktivitas.before),
          after: formatNumber(cardDummy.totalAktivitas.after),
          percentage: getPercentage(Math.abs(cardDummy.totalAktivitas.after-cardDummy.totalAktivitas.before), cardDummy.totalAktivitas.before),
          status: cardDummy.totalAktivitas.before > cardDummy.totalAktivitas.after ? 0 : 1
        },
        transaksiGagal: {
          before: formatNumber(cardDummy.transaksiGagal.before),
          after: formatNumber(cardDummy.transaksiGagal.after),
          percentage: getPercentage(Math.abs(cardDummy.transaksiGagal.after-cardDummy.transaksiGagal.before), cardDummy.transaksiGagal.before),
          status: cardDummy.transaksiGagal.before > cardDummy.transaksiGagal.after ? 0 : 1
        },
        transaksiSukses: {
          before: formatNumber(cardDummy.transaksiSukses.before),
          after: formatNumber(cardDummy.transaksiSukses.after),
          percentage: getPercentage(Math.abs(cardDummy.transaksiSukses.after-cardDummy.transaksiSukses.before), cardDummy.transaksiSukses.before),
          status: cardDummy.transaksiSukses.before > cardDummy.transaksiSukses.after ? 0 : 1
        },
        proyeksiPendapatan: {
          before: formatNumber(cardDummy.proyeksiPendapatan.before),
          after: formatNumber(cardDummy.proyeksiPendapatan.after),
          percentage: getPercentage(Math.abs(cardDummy.proyeksiPendapatan.after-cardDummy.proyeksiPendapatan.before), cardDummy.proyeksiPendapatan.before),
          status: cardDummy.proyeksiPendapatan.before > cardDummy.proyeksiPendapatan.after ? 0 : 1
        }
      };
      setDataCard(res);
    };
  }, [cardDummy]);

  // get dataTable
  useEffect(() => {
    if(tableDummy) {
      const dataArr = tableDummy;
      const len = Math.ceil(tableDummy.length/10);
      const temp = [];
      for(let i=0; i<len; i++) {
        temp.push(dataArr.slice(i*10, (i+1)*10));
      };
      setDataTable(temp);
    };
  }, [tableDummy]);

  // check role on sessionStorage
  useEffect(() => {
    if(sessionStorage) {
      setRole(sessionStorage.getItem('role'));
    };
  }, [sessionStorage]);

  useEffect(() => {

    // searching data
    if(dataSearch || dataFilter) {
      setNoDataModal(true);
    };

    // change chart
    if(dataFilter) {
      setChartType(dataFilter.tabs.tabValue1);
    };

  }, [dataSearch, dataFilter]);

  return (
    <div className={classes.ringkasan}>
      <Title label='Dashboard' >
        {role !== 0 && (
          <Search
            options={['All', 'Transaksi ID', 'Tgl & Waktu', 'Aktivitas', 'Nasabah ID', 'Versi Aplikasi']}
            dataSearch={dataSearch}
            setDataSearch={setDataSearch}
          />
        )}
      </Title>
      {role == 0 ? (
        <div className={classes.center}>
          <img src={bookAccount} alt='Book Account' className={classes.account} />
          <p className={classes.desc}>Selamat Datang [Username] di Back Office BTN </p>
        </div>
      ) : (
        <div className={classes.container}>
          <Filter
            dataFilter={dataFilter}
            setDataFilter={setDataFilter}
            align='center'
            options={[
              {
                id: 1,
                type: 'tabs',
                options: ['Mingguan', 'Bulanan', 'Tahunan']
              },
              {
                id: 2,
                type: 'date',
                placeholder: ['Start Date', 'End Date']
              },
              {
                id: 3,
                type: 'date',
                placeholder: ['Start Date', 'End Date'],
              }
            ]}
          />
          <div className={classes.card}>
            <Card
              color='blue'
              title='Total Aktivitas'
              number1={dataCard && dataCard.totalAktivitas.after}
              number2={dataCard && dataCard.totalAktivitas.before}
              percentage={dataCard && dataCard.totalAktivitas.percentage}
              status={dataCard && dataCard.totalAktivitas.status}
            />
            <Card
              color='red'
              title='Total Transaksi Gagal'
              number1={dataCard && dataCard.transaksiGagal.after}
              number2={dataCard && dataCard.transaksiGagal.before}
              percentage={dataCard && dataCard.transaksiGagal.percentage}
              status={dataCard && dataCard.transaksiGagal.status}
            />
            <Card
              color='green'
              title='Total Transaksi Sukses'
              number1={dataCard && dataCard.transaksiSukses.after}
              number2={dataCard && dataCard.transaksiSukses.before}
              percentage={dataCard && dataCard.transaksiSukses.percentage}
              status={dataCard && dataCard.transaksiSukses.status}
            />
            <Card
              color='purple'
              title='Proyeksi Pendapatan'
              number1={dataCard && dataCard.proyeksiPendapatan.after}
              number2={dataCard && dataCard.proyeksiPendapatan.before}
              percentage={dataCard && dataCard.proyeksiPendapatan.percentage}
              status={dataCard && dataCard.proyeksiPendapatan.status}
              showInfo
            />
          </div>
          <div className={classes.chart}>
            <h1 className={classes.title}>Aktivitas</h1>
            <Chart type={chartType===0 ? 'Mingguan' : chartType===1 ? 'Bulanan' : 'Tahunan'} />
          </div>
          <div>
            <Table
              data={dataTable && dataTable[page-1]}
              cols={tableConfig}
              page={page}
              setPage={setPage}
              totalPages={dataTable && dataTable.length}
              totalData={tableDummy && tableDummy.length}
            />
          </div>
        </div>
      )}
      <NoDataModal
        isOpen={noDataModal}
        handleClose={() => setNoDataModal(false)}
      />
    </div>
  );
};

Ringkasan.propTypes = {};

Ringkasan.defaultProps = {};

export default Ringkasan;